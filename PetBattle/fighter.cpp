#include "fighter.h"
#include "Utility.h"

//definition
Fighter::Fighter(std::string a_name, int a_health, int a_attack, bool a_isFainted)
{
	name = a_name;
	health = a_health;
	attack = a_attack;
	isFainted = a_isFainted;
	murderer = nullptr;
}

Fighter::Fighter()
{

}

Fighter::~Fighter()
{

}

void Fighter::Print()
{
	cout << name << "\tHP: " << health << "    ATK: " << attack << endl;
}

void FighterArray::SetFighters()
{
	for (int i = 0; i < fighterCapacity; i++)
	{
		//make a temp containing a new name
		string temp = GenerateRandomName();
		//while the fighter[i]s name exists, keep generating a new name until it doesnt equal temp
		if (fighters[i] != nullptr) 
		{
			while (fighters[i]->name == temp)
			{
				temp = GenerateRandomName();
			}
		}
		//make a new fighter with the new temp name
		AddFighter(Fighter(temp, maxHealth, rand() % maxAttack, false));
	}
}

FighterArray::FighterArray()
{
	//initialse array of fighters
	fighters = new Fighter*[fighterCapacity];

	//set all addresses to nullptr
	for (int i = 0; i < fighterCapacity; i++)
	{
		fighters[i] = nullptr;
	}

	fighterCount = 0;
	deathCount = 0;
}

FighterArray::~FighterArray()
{
	for (int i = 0; i < fighterCount; i++)
	{
		delete fighters[i];
	}

	delete[] fighters;
}
bool FighterArray::NameCheck(string a_name)
{
	for (int i = 0; i < fighterCapacity; i++)
	{

		if (fighters[i]->name == a_name)
		{
			return true;
		}
		return false;
	}
}

void FighterArray::AddFighter(Fighter newFighter)
{
		Fighter** newMemory = new Fighter*[fighterCapacity * 2];

		for (int i = 0; i < fighterCount; i++)
		{
			//copy item from old array to new one
			if (fighters[i] != nullptr)
			{
				newMemory[i] = fighters[i];
			}
		}
		//delete old memory
		delete[] fighters;

		//make sure items holds address of new memory
		fighters = newMemory;
		for (int i = fighterCount; i < fighterCapacity; ++i)
		{
			newMemory[i] = nullptr;
		}
	
	//will hold the address of the first potential item
	fighters[fighterCount] = new Fighter(newFighter);

	//increment count for next time
	fighterCount++;
}


void FighterArray::RemoveFighter(std::string a_name)
{
	int newCapacity = 0;
	for (int i = 0; i < fighterCount; ++i)
	{
		if (fighters[i]->name != a_name)
		{
			newCapacity++;
		}
	}

	//index used for new memory index. only changed whenver a fighter is copied over.
	int index = 0;
	Fighter** newMemory = new Fighter*[newCapacity];

	for (int i = 0; i < fighterCount; ++i)
	{
		if (fighters[i]->name != a_name)
		{
			newMemory[index] = fighters[i];
			index++;
		}
	}

	fighterCount--;
	//set the fighters pointer to the new memory
	fighters = newMemory;
}

void FighterArray::PrintArray()
{
	for (int i = 0; i < fighterCount; i++)
	{
		if (fighters[i] != nullptr)
		{
			fighters[i]->Print();
		}
		if (fighters[i] != nullptr && fighters[i]->murderer != nullptr)
		{
			cout << "Killed by: " << fighters[i]->murderer->name << endl;
			cout << endl;
		}
	}

}

bool FighterArray::Battle()
{
	//pick two random fighers
	int randFighter01 = rand() % fighterCount;
	int randFighter02 = rand() % fighterCount;

	//check if fainted
	if (fighters[randFighter01]->isFainted == true || fighters[randFighter02]->isFainted == true)
	{
		cout << "\n" << fighters[randFighter01]->name << " and " << fighters[randFighter02]->name << " are already dead so no one attacked." << endl;
	}

	//check if they are trying to hit themselves
	else if (randFighter01 == randFighter02)
	{
		cout << "\n" << fighters[randFighter01]->name << " tried to hit themself.\n";
	}

	//make them attack eachother
	else
	{
		cout << "\n" << fighters[randFighter01]->name << " attacks " << fighters[randFighter02]->name << endl;

		//subtract f2 attack from f1's health
		fighters[randFighter01]->health -= fighters[randFighter02]->attack;

		if (fighters[randFighter01]->health <= 0)
		{
			//reset it back to zero
			fighters[randFighter01]->health = 0;
			//set fainted to true
			fighters[randFighter01]->isFainted = true;
			//increment death count
			deathCount++;
			//point to address of enemy murderer
			fighters[randFighter01]->murderer = fighters[randFighter02];
			cout << fighters[randFighter01]->name << " was brutally murdered by " << fighters[randFighter02]->name << endl;
		}

		//subtract f1 attack from f2's health
		fighters[randFighter02]->health -= fighters[randFighter01]->attack;

		if (fighters[randFighter02]->health <= 0)
		{
			//reset it back to zero
			fighters[randFighter02]->health = 0;
			//set fainted to true
			fighters[randFighter02]->isFainted = true;
			//increment death count
			deathCount++;
			//point to address of enemy murderer
			fighters[randFighter02]->murderer = fighters[randFighter01];
			cout << fighters[randFighter02]->name << " was brutally murdered by " << fighters[randFighter01]->name << endl;
		}

	}
	if (deathCount >= fighterCapacity - 1)
	{
		system("pause");
		system("cls");
		PrintArray();
		return true;
	}
	else
	{
		return false;
	}

}

void FighterArray::SortStats()
{
	bool hasSwapped = true;

	while (hasSwapped)
	{
		//reset swap
		hasSwapped = false;

		for (int i = 0; i < fighterCapacity - 1; i++)
		{
			if (fighters[i]->health > fighters[i + 1]->health)
			{
				//swap them
				Fighter *temp = fighters[i];
				fighters[i] = fighters[i + 1];
				fighters[i + 1] = temp;
				hasSwapped = true;
			}
		}
	}
}

bool FighterArray::GetUserInput()
{
	while (true)
	{
		system("cls");
		PrintArray();
		char choice = ' ';
		std::string fighterName;

		cout << "\n(S)tart Battle, (R)emove Fighter, (A)dd Fighter\n";
		cin >> choice;

		switch (choice)
		{
		case 's':
		case 'S':
			return true;
		case 'r':
		case 'R':
			cout << "Enter name of fighter to remove:";
			cin >> fighterName;
			RemoveFighter(fighterName);
			break;
		case 'a':
		case 'A':
			if (fighterCount == 10)
			{
				cout << "Maximum of 10 fighters";
				Sleep(800);
				break;
			}
			else
			{
				AddFighter(Fighter(GenerateRandomName(), maxHealth, rand() % maxAttack, false));
			}
			break;
		}
	}
}


