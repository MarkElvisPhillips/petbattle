#include "Utility.h"
using namespace std;

string GenerateRandomName()
{
	string fighterNames[15]{ "Lola", "Ging", "Minni", "Finn", "Poko", "You", "Gozu", "Muki", "Pyro", "Jake", "Toph", "Doggo", "Raina", "Shokk", "Gon" };

	int randNameIndex = rand() % 10;

	return fighterNames[randNameIndex];
}
