#include "fighter.h"
#include "Utility.h"
#include <iostream>
#include <string>
#include <ctime>
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  
FighterArray fighterArray;

void MainGameLoop()
{
	srand(time(NULL));
	bool gameover = false;

	fighterArray.SetFighters();
	fighterArray.GetUserInput();

	while (gameover == false)
	{
		//clear the console
		system("cls");

		//Print array of fighters
		fighterArray.PrintArray();

		//Fight eachother
		gameover = fighterArray.Battle();

		if (gameover == true)
		{
			cout << "\nThe battle is over!\n";
			system("pause");
		}

		fighterArray.SortStats();

		system("pause");
	}
}

void main()
{
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	MainGameLoop();
}