#pragma once
#include <iostream>
#include <string>
#include <windows.h>
using namespace std;

struct Fighter
{
	Fighter(std::string a_name, int a_health, int a_attack, bool a_isFainted);
	Fighter();
	~Fighter();
	void Print();

	string name;
	int health;
	int attack;
	bool isFainted;
	Fighter* murderer;
};

class FighterArray 
{
	const int maxHealth = 100;
	const int maxAttack = 80;

	int fighterCapacity = 6;
	int fighterCount;
	int deathCount;
	Fighter** fighters;

public:
	FighterArray();
	~FighterArray();
	void AddFighter(Fighter newFighter);
	void RemoveFighter(std::string a_name);
	void PrintArray();
	bool Battle();
	void SetFighters();
	void SortStats();
	bool GetUserInput();
	bool NameCheck(string a_name);
};
